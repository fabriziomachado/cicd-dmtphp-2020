ARG DOCKER_IMAGE_COMPOSER
ARG DOCKER_IMAGE_PHP

FROM ${DOCKER_IMAGE_COMPOSER} AS composer
FROM ${DOCKER_IMAGE_PHP} AS php

LABEL maintainer="Adiel Cristo <adiel@adielcristo.com>"

ARG APCU_VERSION=5.1.19

# Install PHP extensions and tools
RUN apk update && \
    apk add --no-cache --virtual .build-deps \
        autoconf \
        g++ \
        make && \
    apk add --no-cache --virtual .tool-deps \
        gettext-dev \
        git \
        icu-dev \
        libxml2-dev \
        libzip-dev \
        openssh \
        openssl-dev && \
    docker-php-ext-install -j "$(nproc)" \
        gettext \
        intl \
        mysqli \
        opcache \
        pdo_mysql \
        zip && \
    pecl install \
        apcu-${APCU_VERSION} && \
    docker-php-ext-enable \
        apcu && \
    apk del --purge .build-deps && \
    docker-php-source delete

# Install Composer
COPY --from=composer /usr/bin/composer /usr/bin/composer
ENV COMPOSER_HOME /composer
ENV PATH ${PATH}:/composer/vendor/bin
ENV COMPOSER_ALLOW_SUPERUSER 0
